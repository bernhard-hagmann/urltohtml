package com.bernhardhagmann;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Map;

public class HtmlFile {

	private String file;

	public HtmlFile(String file) {
		this.file = file;
	}

	public void write(Map<String, String> urls) {
		try (PrintWriter pw = new PrintWriter(new FileOutputStream(file), true)) {
			for (Map.Entry<String, String> url : urls.entrySet()) {
				pw.append(createLink(url.getKey(), url.getValue()));
			}
		} catch (FileNotFoundException e) {
			// can never get here
			e.printStackTrace();
		}
	}

	private String createLink(String name, String link) {
		return "<a href=\"" + link + "\" title=\"" + name + "\">" + name + "</a><br/>";
	}

}
