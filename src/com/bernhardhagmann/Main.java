package com.bernhardhagmann;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
		String inDirectory = getArgumentOrDefault(0, args, ".");
		String outFile = getArgumentOrDefault(1, args, "urls.html");

		System.out.println("Reading URL files from " + inDirectory);
		Map<String, String> urls = readUrlFiles(inDirectory);

		System.out.println("Writing HTML output to " + outFile);
		HtmlFile html = new HtmlFile(outFile);
		html.write(urls);
    }

	private static String getArgumentOrDefault(int index, String[] args, String defaultValue) {
		return (args.length >= index + 1)
				? args[index]
				: defaultValue;
	}

	private static Map<String, String> readUrlFiles(String directory) {
		HashMap<String, String> map = new HashMap<>();

		File dir = new File(directory);
		File[] files = dir.listFiles(new FileExtensionFilter("url"));
		for(File file : files) {
			try {
				String url = new UrlFile(file).readUrl();
				map.put(file.getName(), url);
			} catch (IOException e) {
				System.out.println("Could not read file " + file.getName());
				e.printStackTrace();
			}
		}

		return map;
	}

}
