package com.bernhardhagmann;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class UrlFile {

	private File file;

	public UrlFile(File file) {
		this.file = file;
	}

	public String readUrl() throws IOException {
		List<String> lines = Files.readAllLines(file.toPath());

		String url = null;
		for (String line : lines) {
			url = readIncluding("http://", line);
			if (url == null) {
				url = readIncluding("www.", line);
			}
			if (url == null) {
				url = readExcluding("URL=", line);
			}
			if (url != null) {
				break;
			}
		}

		return (url == null) ? "" : url;
	}

	private String readIncluding(String start, String line) {
		return readLine(start, line, false);
	}

	private String readExcluding(String start, String line) {
		return readLine(start, line, true);
	}

	private String readLine(String start, String line, boolean exclude) {
		int startIndex = line.indexOf(start);

		String url = null;
		if (startIndex >= 0) {
			if (exclude) {
				startIndex += start.length();
			}
			url = line.substring(startIndex);
		}
		return url;
	}

}
