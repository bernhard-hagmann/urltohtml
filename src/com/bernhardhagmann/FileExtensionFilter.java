package com.bernhardhagmann;

import java.io.File;
import java.io.FilenameFilter;

public class FileExtensionFilter implements FilenameFilter {

	private String allowedExtension;

	public FileExtensionFilter(String extension) {
		this.allowedExtension = extension.toLowerCase();
	}

	@Override
	public boolean accept(File dir, String name) {
		return getExtension(name).equals(allowedExtension);
	}

	private String getExtension(String name) {
		int dotIndex = name.lastIndexOf('.');
		return (dotIndex >= 0)
				? name.toLowerCase().substring(dotIndex + 1)
				: "";
	}

}
