# UrlToHtml
Creates a HTML file with the links of all *.url files in the given directory.

Usage
-----
`java -jar UrlToHtml.jar [Directory] [OutputFile]`  
`Directory`  ... the directory to read URL files from (Default: .)  
`OutputFile` ... the path and name of the output file (Default: urls.html)